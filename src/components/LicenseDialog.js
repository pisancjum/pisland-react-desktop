import React from "react";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import {Typography} from "@material-ui/core";

export const LicenseDialog = React.forwardRef((props, ref) => (
    <Dialog
        open={props.open}
        onClose={props.onClose}
        scroll="paper"
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
    >
        <DialogTitle disableTypography id="scroll-dialog-title">
            <Typography variant="h3">
                Licencje
            </Typography>
        </DialogTitle>
        <DialogContent dividers>
            <DialogContentText component="div"
                id="scroll-dialog-description"
                ref={ref}
                tabIndex={-1}
            >
                <Typography variant="h5" gutterBottom>
                    Kod źródłowy dostępny na <a href="https://gitlab.com/pisancjum/pisland-react-desktop"
                                                target="_blank" rel="noreferrer">gitlabie</a>
                    <br/><br/>
                </Typography>
                <Typography variant="h4">
                    Released under MIT License
                    <br/><br/>
                </Typography>
                <Typography variant="h5" className="justify-text" gutterBottom>
                    Copyright (c) 2020, Foreplay
                    <br/><br/>
                    Permission is hereby granted, free of charge, to any person obtaining a copy of this software
                    and associated documentation files (the "Software"), to deal in the Software without
                    restriction, including without limitation the rights to use, copy, modify, merge, publish,
                    distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
                    Software is furnished to do so, subject to the following conditions:
                    <br/><br/>
                    The above copyright notice and this permission notice shall be included in all copies or
                    substantial portions of the Software.
                    <br/><br/>
                    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
                    BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
                    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
                    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
                    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
                    <br/><br/>
                </Typography>
                <Typography variant="h5" gutterBottom>
                    Obrazek na tło i favicon znaleziony tutaj: <a href="https://acegif.com/polish-flag-on-gifs/"
                                                                  target="_blank"
                                                                  rel="noreferrer">https://acegif.com/polish-flag-on-gifs/</a>
                </Typography>
                <Typography variant="h5" gutterBottom>
                    <a href="https://www.npmjs.com/package/react" target="_blank" rel="noreferrer">React:
                        17.0.1</a> (MIT)
                </Typography>
                <Typography variant="h5" gutterBottom>
                    <a href="https://www.npmjs.com/package/react-dom" target="_blank" rel="noreferrer">React DOM:
                        17.0.1</a> (MIT)
                </Typography>
                <Typography variant="h5" gutterBottom>
                    <a href="https://www.npmjs.com/package/react-router-dom" target="_blank" rel="noreferrer">React
                        Router DOM: 5.2.0</a> (MIT)
                </Typography>
                <Typography variant="h5" gutterBottom>
                    <a href="https://www.npmjs.com/package/recharts" target="_blank" rel="noreferrer">Recharts:
                        1.8.5</a> (MIT)
                </Typography>
                <Typography variant="h5" gutterBottom>
                    <a href="https://www.npmjs.com/package/@material-ui/core" target="_blank" rel="noreferrer">Material
                        UI Core: 4.11.2</a> (MIT)
                </Typography>
                <Typography variant="h5" gutterBottom>
                    <a href="https://www.npmjs.com/package/@material-ui/icons" target="_blank" rel="noreferrer">Material
                        UI Icons: 4.11.2</a> (MIT)
                </Typography>
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose} color="primary">
                Zamknij
            </Button>
        </DialogActions>
    </Dialog>
));