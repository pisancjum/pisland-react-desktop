import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import {Typography} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

export const PolicyDialog = React.forwardRef((props, ref) => (
    <Dialog
        open={props.open}
        onClose={props.onClose}
        scroll="paper"
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
    >
        <DialogTitle disableTypography id="scroll-dialog-title">
            <Typography variant="h3">
                Polityka Prywatności Pislandu
            </Typography>
        </DialogTitle>
        <DialogContent dividers>
            <DialogContentText component="div"
                id="scroll-dialog-description"
                ref={ref}
                tabIndex={-1}
            >
                <Typography variant="h5" gutterBottom>
                    Nie zbieramy i nie przechowywujemy żadnych informacji osobowych na stronie Pislandu.
                    Nie ma też reklam i śledzenia. Nie używamy cookies.
                </Typography>
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose} color="primary">
                Zamknij
            </Button>
        </DialogActions>
    </Dialog>
));
