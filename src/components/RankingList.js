import React from "react";
import {RankingRow} from "./RankingRow";
import {rankings} from '../data/Rankings'

export function RankingList() {

    return rankings.map((data, i) => <React.Fragment key={i}>
            <RankingRow data={data}/>
        </React.Fragment>)
}